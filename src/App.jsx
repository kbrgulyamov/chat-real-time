import { useState } from "react";
import Header from "./Components/Header/Header";
import "./App.css";
import { BrowserRouter } from "react-router-dom";
import AppRouter from "./Components/AppRouter/AppRouter";

function App() {
  const [count, setCount] = useState(0);

  return (
    <React.Fragment>
      <BrowserRouter>
        <Header />
        <AppRouter />
      </BrowserRouter>
    </React.Fragment>
  );
}

export default App;
