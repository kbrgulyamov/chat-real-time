import Chat from "../Chat/Chat";
import UserLogin from "../Login/UserLogin";
import { CHAT_ROUTE, LOGIN_ROUTE } from "../utils/const";

export const publickRoutes = [
  {
    path: LOGIN_ROUTE,
    Component: UserLogin,
  },
];

export const privateRoutes = [
  {
    path: CHAT_ROUTE,
    Component: Chat,
  },
];
