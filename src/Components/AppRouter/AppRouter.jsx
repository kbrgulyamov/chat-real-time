import { Route, Switch, Redirect } from "react-router-dom";
import { privateRoutes, publickRoutes } from "../Routes/routes";
import { CHAT_ROUTE, LOGIN_ROUTE } from "../utils/const";

const AppRouter = () => {
  const user = false;
  return user ? (
    <Switch>
      {privateRoutes.map(({ path, Component }) => (
        <Route path={path} component={Component} exact={true} />
      ))}
      <Redirect to={CHAT_ROUTE} />
    </Switch>
  ) : (
    <Switch>
      {publickRoutes.map(({ path, Component }) => (
        <Route path={path} component={Component} exact={true} />
      ))}
      <Redirect to={LOGIN_ROUTE} />
    </Switch>
  );
};

export default AppRouter;
